import java.util.* ;

    public class Main3 {

        public static void main ( String[] args) {

            ArrayList<String> stringid = new ArrayList<String>();

            stringid.add("Luule");
            stringid.add("Maali");
            stringid.add("Tiiu");

            for ( int j=0; j<stringid.size(); j++ )
                System.out.println("element " + j + ": " + stringid.get(j) );
    }

}


//Ülesanne 3:
//Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis tagastab täisarvude massiivi.
// Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi
// (stringi) pikkust. Meetodi nime võid ise välja mõelda.
// Kutsu see meetod main()-meetodist välja ja prindi tulemus (kõik massiivi elemendid) standardväljundisse.