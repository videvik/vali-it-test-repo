public class Country {
    private int Population;
    private String Name;

    public Country() {

    }

    public int getPopulation () {
        return Population;
    }

    public void setPopulation ( int population ) {
        Population = population;
    }

    public String getName () {
        return Name;
    }

    public void setName ( String name ) {
        Name = name;
    }
}


// Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName, setName ja
// list riigis enim kõneldavate keeltega.
//
//Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti,
// mis sisaldaks endas kõiki parameetreid väljaprindituna.
//
//Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja
// prindi selle riigi info välja .toString() meetodi abil.