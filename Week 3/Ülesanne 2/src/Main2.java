public class Main2 {
    public boolean vordsed ( int x , int y ) {
        if ( x == y ) {
            return true;
        } else {
            return false;
        }

    }
}

// Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks täisarvulist muutujat.
// Meetod tagastab tõeväärtuse vastavalt sellele, kas kaks sisendparametrit on omavahel võrdsed või mitte.
// Meetodi nime võid ise välja mõelda.
//
//Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.