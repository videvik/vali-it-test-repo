public class Main4 {
    //public static byte sajand (int aastaarv) {

   // Kui funktsioonile antakse ette parameeter, mis on kas suurem,
// kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1;
//
    static void sajand(int aasta) {

        // No negative value is allow for year
        if (aasta <= 0 || aasta >= 2018 )
            System.out.print("-1");

            // If year is between 1 to 100 it
            // will come in 1st century
        else if (aasta <= 100)
            System.out.print("1ne sajand\n");

        else if (aasta % 100 == 0)
            System.out.print(aasta / 100 + " sajand");

        else
            System.out.print(aasta / 100 + 1 + " sajand");
    }

    // Driver code
    public static void main(String[] args) {
        int aasta = 0;
        sajand(aasta);
    }
}
// Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int) vahemikus 1 - 2018
// ja tagastab täisarvu (byte) vahemikus 1 - 21 vastavalt sellele, mitmendasse sajandisse antud aasta kuulub.
// Meetodi nime võid ise välja mõelda. Kui funktsioonile antakse ette parameeter, mis on kas suurem,
// kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1;
//
//Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega: 0, 1, 128, 598, 1624, 1827, 1996, 2017.
// Prindi need väärtused standardväljundisse.