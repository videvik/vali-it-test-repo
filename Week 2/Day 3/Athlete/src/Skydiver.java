
public class Skydiver extends Athlete { //alamklass, mis pärineb sportlase klassist
    public Skydiver(String perenimi) { //see siin konstruktor, selle saab ka automaatselt
        super(perenimi);
    }
    @Override
    public void perform () {
        System.out.println(perenimi + ": langen, langen, langen");
    }
}
