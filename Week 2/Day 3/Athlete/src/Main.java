import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Athlete> sportlased = new ArrayList<>();
            sportlased.add(new Skydiver("Kukk"));  //new on uue atribuudi kohta
            sportlased.add(new Skydiver("Kana"));
            sportlased.add(new Skydiver("Hani"));
            sportlased.add(new Runner("Tiiger"));
            sportlased.add(new Runner("Puuma"));
            sportlased.add(new Runner("Gepard"));


        //langevarjur1.getClass().getDeclaredFields();

        for (Athlete sportlane:sportlased) {
            System.out.println(sportlane.eesnimi);
            System.out.println(sportlane.perenimi);
            System.out.println(sportlane.vanus);
            System.out.println(sportlane.sugu);
            System.out.println(sportlane.pikkus);
            System.out.println(sportlane.kaal);
        }
    }
}
