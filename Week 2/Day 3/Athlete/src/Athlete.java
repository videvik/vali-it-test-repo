

public abstract class Athlete {  //defineeri klass sportlane
     String eesnimi; //siin omadused ehk atribuudid
     String perenimi;
     Integer vanus;
     String sugu;
     Double pikkus;
     Double kaal;

    public Athlete(String perenimi) {
        this.perenimi=perenimi;
    }


    public abstract void perform(); //abstraktse meetodi lõpus on semikoolon, abstraktsest ei saa objekti luua (new athlete).
    // Kui keegi extendib seda athlete, siis tuleb kindlasti perform meetodit kirjeldada
    // Saan nt teha kindlaks enne kui progr töötab, et kõikidel sportlastel on need omadused olemas. Et kogi info oleks igal pool ühesuguses formaadis


}
