package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin  // lubab kõiki tyypi päringuid (kuigi pole väga turvaline)
public class APPIController {
    Chatroom general= new Chatroom("general");

    @GetMapping("/chat/general")
    Chatroom chat(){
        return general;
    }
    @PostMapping("chat/general/new-message")
    void newMessage(@RequestBody ChatMessage msg) {
        general.addMessage(msg);
    }

}
