package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {
    private String user;
    private String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;
    private String picture;

    public ChatMessage(){

    }

    public ChatMessage (String user, String message, String picture) {
        this.user = user;
        this.message = message;
        this.date = new Date();
        this.picture = picture;
    }

    // sõnum väljastab serveris ka profiilipildi urli (peale useri ja message'i lisa ka profiilipildi url, mis salvestub serverisse ja ka kuvatakse HTMLis välja)
    // Nimelt, et kasutaja saab kolmanda välja, kuhu url panna.
    // lisaks luua uus chatroom peale "general"



    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getPicture() {
        return picture;
    }
}
