public class Main {  // main on meetod, kus kutsume välja teises meetodis välja kutsutu

    public static void main(String[] args) {

        System.out.println("Hello World!");//console.log

        System.out.println("sout"); // sout on kiirkäsk    //iga rea lõpus semikoolon

        int number = 5; // int on 4 byte, TÄISARV
        double n2 = 5.1;
        byte bait = 5; // kokku 255 väärtust, -128 to 127 (8bit == 1byte)
        //primitiiv tyypi muutujad
        String nimi = "Karmen"; //objekt tyypi muutuja     SÕNE
        char algusTaht = 'K'; // ÜKSIK TÄHT


        if (number == 5) { // If SÜNTAKS SAMA, mis jsis
        } else {

        }

        int liitmine = (int) (number + n2); //number on int ja n2 on double, kokku tuleb double; sulgudes int castib yhest tyybist teise
        System.out.println(liitmine);
        System.out.println(Math.round(10.7)); // ymardamiseks eraldi meetod

        int parisNumber = Integer.parseInt("4");
        double parisNumber2 = Double.parseDouble("4.1");

        String puuvili1 = "Banaan";
        String puuvili2 = "Apelsin";
        if (puuvili1 == puuvili2) {
            System.out.println("Puuviljad on võrdsed!!!");
        } else {
            System.out.println("Ei ole võrdsed!");
        }

        Koer.auh();
        Koer.lausu();

    }
    public static int summa(int a, int b) {
            return a + b;
        }

    }



    //Object != object//
