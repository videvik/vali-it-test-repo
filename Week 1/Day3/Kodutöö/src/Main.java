import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) { // klassid on staatilised, objektid on dünaamilised (seal saavad muutuda väärtused)
        double num1 = 5.12;
        double num2 = 7;
        double num3 = 12.2;
        System.out.println(KlassMeetod.ruut (num1));
        System.out.println(KlassMeetod.ruut (num2));
        System.out.println(KlassMeetod.ruut (num3));

        int tulemus = KlassMeetod.astendaja(2,7);
        System.out.println(tulemus);
      KlassMeetod.getVanus(); //viitab klassile, punkt ühendab neid, getVanus viitab getVanuse meetodi käivitamisele, sulud on meetodi käivitamiseks ja sisendi andmiseks
        System.out.println(KlassMeetod.getVanus());

    KlassMeetod kt = new KlassMeetod();
    System.out.println(kt.vanus);
    kt.korrutaja(2);
    System.out.println(kt.vanus);
    }
}
