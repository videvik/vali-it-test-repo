import nadalapaevad.Reede;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, world!");

        if (true && true){  //boolean
            System.out.println("Tõene");
        }
        if (true && true || !false && true) {  //boolean
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }
        if (false || false && true || false) {  //boolean
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }

        Reede.koju(); // reede peal alt enter saad uue klassi luua, koju peal vajutades loob uue meetodi reede klassi
        Laupaev.peole();

        Pyhapaev.hommik();

        Pyhapaev paev = new Pyhapaev();
        paev.maga();
        paev.hommik();


    }
}
