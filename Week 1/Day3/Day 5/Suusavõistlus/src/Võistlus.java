import java.util.ArrayList;

public class Võistlus {
    ArrayList<Suusataja> voistlejad;
    int distants;

    public Võistlus(){
        System.out.println("Start!");
        voistlejad = new ArrayList();
        distants = 20;
        for (int i = 0; i < 50; i++) {
            int voistleja = i;
            voistlejad.add (new Suusataja(i)); //i on siin siis võistleja number

        }
        aeg();
    }
    public void aeg () {
        for (Suusataja s: voistlejad) {
            s.suusata();
           boolean lopetanud = s.kasOnLopetanud (distants);
           if (lopetanud) {
               System.out.println("Võitja on: " + 2);
               return;
           }


        }
        System.out.println(voistlejad);


        try {
            Thread.sleep(100);  // thread.sleep paneb magama üheks sekundiks
        } catch (InterruptedException e) {  // error handling, et ei jookseks kokku, vaid läheks edasi
            e.printStackTrace();

        }
        aeg();
    }
}
