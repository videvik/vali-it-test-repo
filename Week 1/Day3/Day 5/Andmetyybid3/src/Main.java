import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hi!");

        // ÜL:
        //1. Loo kolm muutujat numbritega
        //2. Moodusta lause nende muutujatega
        //3. Prindi see lause välja

        int aasta = 88;
        int kuu = 9;
        int paev = 16;

        String lause = "Krister sündis aastal " + aasta + ". Kuu oli " + kuu + " ja päev oli " + paev + ".";
        System.out.println(lause);
        // %d on täisarv
        // %f on komakohaga
        // %s on string

        String parem = String.format("Mina sündisin aastal %d. Kuu oli %d ja päev oli %d", aasta, kuu, paev);
        System.out.println(parem);

        // Hashmapis nö kaks tulpa (võib olla ka kaks sõna)
        //ÜL1. Loe kokku, mitu lampi on klassis ja pane see info HashMappi!
        //ÜL2. Loe kokku, mitu akent on klassis ja pane see info HashMappi!

        //ÜL1. Loe kokku, mitu linimest on klassis ja pane see info HashMappi!

        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put("aknad", 5); // aknad on võti, number on väärtus
        klassiAsjad.put("lambid", 11);
        klassiAsjad.put("inimesed", 20);


        System.out.println(klassiAsjad);

        //ÜL: Mõtle välja kolma praktilist kasutust Hashmapile
        // 1. Haiglas on {patsiendiID: nimi}
        // 2. Trammis on {roheliseKaardiId: kontoJääk}
        // 3. Rühmas on {kuiPaljuLapsi: arv}

        // ÜL: Prindi välja kui palju on klassis inimesi, kasutades juba loodud hashmappi.
       int inimesi = (int) klassiAsjad.get("inimesed");
        System.out.println(inimesi);

        // ÜL: Lisa samasse hashmappi juurde, mitu tasapinda on klassis, aga number enne ja siis String/
        // mt {10: "tasapinnad"}/
        klassiAsjad.put(10, "tasapinnad");
        System.out.println(klassiAsjad);

        // ÜL: Loo uus hashmap, kuhu saab sisestada ainult String:double paare. Sisesta sinna ka midagi.

        HashMap<String, Double>uus = new HashMap();
        uus.put("midagi", 5.0);
        System.out.println(uus);

        // ÜL: switch

        int rongiNr = 50;
        String suund = null;
        switch (rongiNr) {
            case 50: suund = "Pärnu"; break; // break selleks, et järgmine ei käivituks, muidu tuleb vormsi vastuseks
            case 55: suund = "Haapsalu"; break;
            case 10: suund = "Vormsi"; break;
        }
        System.out.println(suund);

        // FOREACH

        int [] mingidNumbrid = new int[]{8, 4, 6, 345, 9878, 12345235};
        for (int i = 0; i <mingidNumbrid.length; i++) {
            System.out.println(mingidNumbrid[i]);
        }
        System.out.println("--------------------------");

        for (int nr : mingidNumbrid) {
            System.out.println(nr);
        }

        // ÜL: Õpilane saab töös punkte 0-100
        // 1. Kui punkte on alla 50, kukub ta töö läbi, vastasel juhul on töö hinne täisarvuline punktid/20
        //100 punkti => 5 jne

        int punkte = 99;
        if (punkte > 100 || punkte < 0){
            throw new Error();
        }
        switch ((int)Math.round(punkte/20.0)) {
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");















                ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("ee...õppisid ka?");
                break;
            default:
                System.out.println("Kukkusid läbi");
        }
    }
}
