import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {  // keskseks teemaks on array ehk massiiv
        int[] massiiv = new int[6];  // massiiv on nimekiri, selle pikkus on lõplik, siin 6, muuta seda ei saa (arraylist on veniv)
        ArrayList list = new ArrayList();


        // ÜL: prindi välja massiiv
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);

        // ÜL: määrata kolmandal positsioonil olev number massiivis number 5ks
        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv));

        // ÜL: Prindi välja kuues element massiivist, aga enne määra talle ka väärtus

        massiiv[5] = 7;
        System.out.println(massiiv[5]); // eelmises on arrays.tostring, sest vaja terve jada välja printida

        // ÜL: Prindi välja viimane element, ükskõik kui pikk massiiv ka ei oleks

        int viimane = massiiv[massiiv.length - 1];
        System.out.println("Viimane on " + viimane);

        // ÜL: Loo uus massiiv, kus on kõik 8 numbrit kohe määratud

        int[] massiiv2 = new int[]{1, 2, 3, 4, 55, 6, 7, 99};

        System.out.println(Arrays.toString(massiiv2));

        // ÜL: Prindi välja ükshaaval kõik väärtused massiiv2-st

        int index = 0; //
        while (index < massiiv2.length) {
            System.out.println(massiiv2[index]); // esimese ringiga muutub 1-ks, siin on positsioon
            index++; // sama, mis index = index + 1
        }

        // ÜL: Teeme sama tsükli kiiremini for tsükli
        for (int i = 0; i < massiiv2.length; i++) { //
            System.out.println(massiiv2[i]);

        }

        // ÜL: 1. Loo stringide massiiv, mis on alguses tühi
        // 2. Siis lisad ka keskele ühe sõne.

        String[] stringid = new String[3]; // hakkad muutujat looma, mis on massiivi tüüpi, selle nimi on stringid ja sinna sisse teed uue stringtyypi massiivi ja seal sees on nii mitu kohta
        stringid[1] = "Lõuna"; //stringid nimelise massiivi teisel kohal (nr1 positsioon) määratakse nimeks lõuna
        System.out.println(Arrays.toString(stringid)); // massiivi ei saa printida, selleks on ekstra meetod


        // ÜL: 1. Loo massiiv, kus on sada kohta
        // 2. Täida massiiv järjest loeteluga alustades numbrist 0
        //3. Prindi välja see megamassiiv

        int[] sadaKohta = new int[100]; // tekkis mällu megamass {0, 0, 0.....
        for (int i = 0; i < sadaKohta.length; i++) { //siin valem
            System.out.println(i); //siin prindib välja i väärtuse iga kord (see kõik eelnev loogelistes sulgudes), see pmslt ei pea olema
            sadaKohta[i] = i;

        }
        System.out.println(Arrays.toString(sadaKohta)); // siin prindib välja massiivi

        // ÜL: 1. Kaswuta sadaKohta massiivi, kus on numbrite jada
        // 2. Loe, mitu paarisarvu on?
        // 3. Prindi tulemus välja
        // Kasutada tuleb nii tsüklit kui ka if-lauset
        // Kui jagad kaks arvu operaatoriga %, siis see tagastab jäägi.
        // x % 2, et 0 puhul on paarisarv ja 1 puhul ei ole

        //1. Võtan esimese numbri massiivist, See on 0.
        //2. Ma küsin, kas see number on paaris või mitte?
        //3. Kui on paaris, siis liidan ühe loendajale otsa

        // Algoritm:
        //1. Võtan esimese numbri massiivist, See on 0.
        // 2. Kui number on paaris?
        // 3. Siis liidan ühe loendajale otsa
        //

        int mituPaarisarvuOn = 0;
        for (int i = 0; i < sadaKohta.length; i++) {
            System.out.println(i);
            if (i % 2 == 0) {
                mituPaarisarvuOn++;
            }

        }
        System.out.println("mituPaarisarvuOn: " + mituPaarisarvuOn);

        // Ül. Loo Arraylist ja sisesta sinna kolm numbrit ja kaks stringi


        ArrayList list2 = new ArrayList();
        list2.add(4);
        list2.add(8);
        list2.add(9);
        list2.add("Tere ");
        list2.add("hommikust ");


        // Küsi viimasest listist välja kolmas element ja prindi välja!

        System.out.println(list2.get(2));

        // Prindi kogu list välja

        System.out.println(list2);

        // Prindi iga element ükshaaval välja


        for (int i = 0; i < list2.size(); i++)  //i on 0, kas i on väiksem kui listi suurus ehk kas 0 on väiksem kui 5?, ja siis käivitan, kui tõene
            System.out.println(list2.get(i)); // esimesel ringil prindib välja 0 positsioonil oleva elemendi, mis on 4

        // ÜL 1. Loo uus ArrayList, kus on 543 numbrit (numbrid peavad olema suvalised - Math.random() vahemikus 0-10)
        //2. Siis korruta iga number 5-ga
        // 3. Salvesta see uus number samale positsioonile



        ArrayList list3 = new ArrayList();  // Kui teha nii: ArrayList<Integer> list3 = new Arraylist(); siis ütleb, et siin on ainult integer tyypi (ehk kõik double'id intid)
        for (int i = 0; i < 543; i++) {
            double nr = Math.floor(Math.random() * 11);
            list3.add(nr);
        }
        System.out.println("Algne massiiv: " + list3);

        for (int i = 0; i < list3.size() ; i++) { // stringil on length ja massiivil size
            double nr = (double)list3.get(i) * 5; // siin saab uuesti nr panna, sest see muutuja ei lähe kastist välja // double asemel võib ka int olla
           //võib ka nii: double muudetudNr = nr * 5;
            list3.set(i, nr);
        }
        System.out.println("Muudetud massiiv: " + list3);

    }
}